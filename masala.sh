#!/bin/bash


#adjusting variables
chunk_size=2		#number of lines to be copied from file inside previous dir. to patch concat	
first_line_num=1	#number of head lines to be copied from a file inside a dir.

#script variables
missing=0		#number of consecutive missing directories 	
chunk_start=0		#first line to copy from previous dir.	
chunk_stop=0 		#last line to copy from previous dir.
j=0			#previous dir. index



#programa name and version 
echo "masala v0.1 10/06/2014 by SimianTheAnt"

#get list of complete directories inside default file "index.txt" and build an array of it
dirs=($(cat index.txt))

echo "index.txt: ${dirs[*]}"

#get list of file from the first dir. list
files=$(ls ${dirs[0]})

#get total number of dir. inside "index.txt"
dirs_num=${#dirs[*]}

echo "number of dir(s): $dirs_num"
echo $files

#clean concat/
rm -fr concat/*

#iterate through dirs array and search for files listed in the first dir.
for ((i=0; i<dirs_num; i++ ))
do
 dir=${dirs[i]}
 echo "* seaching for directory $dir..."
 if [ -d $dir ]
  then 
   echo "* dir $dir exists, processing it..."
   for file in ${files[*]}
   do
    echo "* copying first $first_line_num line(s) from file $dir/$file to concat/$file."   
    head -$first_line_num $dir/$file >> concat/$file   
   done  
   let missing=0

  else 
   echo -n "* $dir is missing, patching from previous dir files "
   let missing=missing+1
   echo "(missing depth is $missing)..."
   let chunk_start=missing*chunk_size+1
   let chunk_stop=chunk_start+chunk_size-1 
   for file in ${files[*]} 
   do
    let j=i-missing #index for the previous directory
    echo "* patching concat/$file from file ${dirs[j]}/$file $chunk_size line(s), from line(s) $chunk_start to $chunk_stop"
    echo "*  FROM FILE ${dirs[j]}/$file, $chunk_start:$chunk_stop BEGIN *" >> concat/$file
    sed -n "$chunk_start,$chunk_stop p" ${dirs[j]}/$file >> concat/$file 
    echo "*  FROM FILE ${dirs[j]}/$file, $chunk_start:$chunk_stop END   *" >> concat/$file
   done  
 fi
done

echo "* file concatenation done!"

echo "* splitting files in concat/ based on fields..."

#would purge files from comments
#grep -v^C^*' AB1.txt

awk '{print $1>FILENAME".F1"; print $2>FILENAME".F2"; print $3>FILENAME".F3"; print $4>FILENAME".F4";  print $5>FILENAME".F5";  print $6>FILENAME".F6";  print $7>FILENAME".F7";  print $8>FILENAME".F8";  print $9>FILENAME".F9";  print $10>FILENAME".F10";  print $11>FILENAME".F11";}' concat/*

echo "* field files generated. just hava a look at concat/*.F[1-12] files"
 
